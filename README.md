# Mastermind - Microservice Java

This is a simple microservice built with Spring Boot.


A running instance of the project can be found on [OpenShift](http://mastermind-mastermind.apps.us-east-2.starter.openshift-online.com/api/swagger-ui.html).
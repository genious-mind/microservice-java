package com.gamesportal.mastermind.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamesportal.mastermind.bean.MatchConfigurationRequest;
import com.gamesportal.mastermind.bean.MatchResponse;
import com.gamesportal.mastermind.enumeration.Style;
import com.gamesportal.mastermind.exception.ConfigurationNotValidException;
import com.gamesportal.mastermind.exception.handler.RestExceptionHandler;
import com.gamesportal.mastermind.service.GameService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.gamesportal.mastermind.MatchTestUtils.ACCOUNT_CODE;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(MockitoExtension.class)
public class GameController_createMatch_Test {

    private MockMvc mvc;

    @Mock
    private GameService gameService;

    @Mock
    private MessageSource messageSource;

    private JacksonTester<MatchResponse> matchParser;

    private JacksonTester<MatchConfigurationRequest> configParser;

    @BeforeEach
    public void init() {
        JacksonTester.initFields(this, new ObjectMapper());

        mvc = MockMvcBuilders
                .standaloneSetup(new GameController(gameService))
                .setControllerAdvice(new RestExceptionHandler(messageSource))
                .build();
    }

    @Test
    public void no_account() throws Exception {
        MockHttpServletResponse response = mvc.perform(post("/match/")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void void_account() throws Exception {
        MockHttpServletResponse response = mvc.perform(post("/match//")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void invalid_account() throws Exception {

        MockHttpServletResponse response = mvc.perform(post("/match/123456-asdasfa-12334566-sfsdfsdf/")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void null_body() throws Exception {
        MockHttpServletResponse response = mvc.perform(post("/match/" + ACCOUNT_CODE + "/").content("")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void empty_object() throws Exception {
        MockHttpServletResponse response = mvc.perform(post("/match/" + ACCOUNT_CODE + "/").contentType(MediaType.APPLICATION_JSON).content("{}")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void empty_config() throws Exception {
        MockHttpServletResponse response = mvc.perform(post("/match/" + ACCOUNT_CODE + "/").contentType(MediaType.APPLICATION_JSON)
                .content(configParser.write(new MatchConfigurationRequest()).getJson()))
                .andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void wrong_style() throws Exception {
        MockHttpServletResponse response = mvc.perform(post("/match/" + ACCOUNT_CODE + "/").contentType(MediaType.APPLICATION_JSON)
                .content("{\"style\": \"FOO\"}"))
                .andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void null_style() throws Exception {
        MatchConfigurationRequest matchConfigurationRequest = new MatchConfigurationRequest();
        matchConfigurationRequest.setMaxDigit(6);
        matchConfigurationRequest.setNumberLength(5);

        MockHttpServletResponse response = mvc.perform(post("/match/" + ACCOUNT_CODE + "/").contentType(MediaType.APPLICATION_JSON)
                .content(configParser.write(matchConfigurationRequest).getJson()))
                .andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void null_maxDigit() throws Exception {
        MatchConfigurationRequest matchConfigurationRequest = new MatchConfigurationRequest();
        matchConfigurationRequest.setStyle(Style.COLOR);
        matchConfigurationRequest.setNumberLength(5);

        MockHttpServletResponse response = mvc.perform(post("/match/" + ACCOUNT_CODE + "/").contentType(MediaType.APPLICATION_JSON)
                .content(configParser.write(matchConfigurationRequest).getJson()))
                .andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void null_numberLength() throws Exception {
        MatchConfigurationRequest matchConfigurationRequest = new MatchConfigurationRequest();
        matchConfigurationRequest.setStyle(Style.COLOR);
        matchConfigurationRequest.setMaxDigit(6);

        MockHttpServletResponse response = mvc.perform(post("/match/" + ACCOUNT_CODE + "/").contentType(MediaType.APPLICATION_JSON)
                .content(configParser.write(matchConfigurationRequest).getJson()))
                .andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void bad_configuration() throws Exception {
        given(gameService.createNewMatch(ACCOUNT_CODE, Style.COLOR, 5, 7))
                .willThrow(new ConfigurationNotValidException(Style.COLOR, 5, 7));

        MatchConfigurationRequest matchConfigurationRequest = new MatchConfigurationRequest();
        matchConfigurationRequest.setStyle(Style.COLOR);
        matchConfigurationRequest.setMaxDigit(5);
        matchConfigurationRequest.setNumberLength(7);

        MockHttpServletResponse response = mvc.perform(post("/match/" + ACCOUNT_CODE + "/").contentType(MediaType.APPLICATION_JSON)
                .content(configParser.write(matchConfigurationRequest).getJson()))
                .andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

}
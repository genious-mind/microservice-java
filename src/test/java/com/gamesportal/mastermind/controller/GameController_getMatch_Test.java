package com.gamesportal.mastermind.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamesportal.mastermind.bean.MatchResponse;
import com.gamesportal.mastermind.entity.Match;
import com.gamesportal.mastermind.enumeration.Status;
import com.gamesportal.mastermind.exception.MatchNotFoundException;
import com.gamesportal.mastermind.exception.handler.RestExceptionHandler;
import com.gamesportal.mastermind.service.GameService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.gamesportal.mastermind.MatchTestUtils.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(MockitoExtension.class)
public class GameController_getMatch_Test {

    private MockMvc mvc;

    @Mock
    private GameService gameService;

    @Mock
    private MessageSource messageSource;

    private JacksonTester<MatchResponse> jsonParser;

    @BeforeEach
    public void init() {
        JacksonTester.initFields(this, new ObjectMapper());

        mvc = MockMvcBuilders
                .standaloneSetup(new GameController(gameService))
                .setControllerAdvice(new RestExceptionHandler(messageSource))
                .build();
    }

    @Test
    public void no_account() throws Exception {
        MockHttpServletResponse response = mvc.perform(get("/match/")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void void_account() throws Exception {
        MockHttpServletResponse response = mvc.perform(get("/match//")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void empty_account() throws Exception {
        MockHttpServletResponse response = mvc.perform(get("/match/ /")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void invalid_account() throws Exception {

        MockHttpServletResponse response = mvc.perform(get("/match/123456-asdasfa-12334566-sfsdfsdf/")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void do_post() throws Exception {
        MockHttpServletResponse response = mvc.perform(post("/match/" + ACCOUNT_CODE + "/")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

    @Test
    public void valid_account() throws Exception {
        Match match = createMatch(ACCOUNT_CODE, Status.OPEN);
        given(gameService.findLastMatch(ACCOUNT_CODE)).willReturn(match);

        MockHttpServletResponse response = mvc.perform(get("/match/" + ACCOUNT_CODE + "/")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assertions.assertNotNull(jsonParser.write(new MatchResponse(match)), response.getContentAsString());
    }

    @Test
    public void account_not_found() throws Exception {
        given(gameService.findLastMatch(ANOTHER_ACCOUNT_CODE)).willThrow(new MatchNotFoundException(ANOTHER_ACCOUNT_CODE));

        MockHttpServletResponse response = mvc.perform(get("/match/" + ANOTHER_ACCOUNT_CODE + "/")).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
        Assertions.assertNotNull(response.getContentAsString());
    }

}
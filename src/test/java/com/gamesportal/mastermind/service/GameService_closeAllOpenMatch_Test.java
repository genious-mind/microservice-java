package com.gamesportal.mastermind.service;

import com.gamesportal.mastermind.entity.Match;
import com.gamesportal.mastermind.enumeration.Status;
import com.gamesportal.mastermind.exception.MatchNotFoundException;
import com.gamesportal.mastermind.repository.MatchRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;

import static com.gamesportal.mastermind.MatchTestUtils.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class GameService_closeAllOpenMatch_Test {

    private final TestEntityManager entityManager;
    private final GameService gameService;

    @Autowired
    public GameService_closeAllOpenMatch_Test(TestEntityManager entityManager, MatchRepository matchRepository) {
        this.entityManager = entityManager;
        this.gameService = new GameService(matchRepository);
    }

    @Test
    public void close_no_match() {
        Assertions.assertThrows(MatchNotFoundException.class, () -> gameService.closeAllOpenMatch(ACCOUNT_CODE));
    }

    @Test
    public void close_one_match() {
        Match openedMatch = createMatch(ACCOUNT_CODE, Status.OPEN);
        entityManager.persist(openedMatch);

        gameService.closeAllOpenMatch(ACCOUNT_CODE);

        assertIsClosed(openedMatch);
    }

    @Test
    public void close_two_match() {
        Match openedMatch1 = createMatch(ACCOUNT_CODE, Status.OPEN);
        Match openedMatch2 = createMatch(ACCOUNT_CODE, Status.OPEN);
        entityManager.persist(openedMatch1);
        entityManager.persist(openedMatch2);

        gameService.closeAllOpenMatch(ACCOUNT_CODE);

        assertIsClosed(openedMatch1);
        assertIsClosed(openedMatch2);
    }

    @Test
    public void close_only_account_match() {
        Match openedMatch1 = createMatch(ACCOUNT_CODE, Status.OPEN);
        Match openedMatch2 = createMatch(ANOTHER_ACCOUNT_CODE, Status.OPEN);
        entityManager.persist(openedMatch1);
        entityManager.persist(openedMatch2);

        gameService.closeAllOpenMatch(ACCOUNT_CODE);

        assertIsClosed(openedMatch1);
        Match closedMatch = entityManager.find(Match.class, openedMatch2.getCode());
        Assertions.assertNotNull(closedMatch);
        Assertions.assertEquals(Status.OPEN, openedMatch2.getStatus());
    }

    @Test
    public void does_not_close_again() {
        Match closedMatch = createMatch(ACCOUNT_CODE, Status.CLOSE);
        Match openedMatch = createMatch(ACCOUNT_CODE, Status.OPEN);
        entityManager.persist(closedMatch);
        entityManager.persist(openedMatch);

        Date now = new Date();
        gameService.closeAllOpenMatch(ACCOUNT_CODE);

        closedMatch = entityManager.find(Match.class, closedMatch.getCode());
        openedMatch = entityManager.find(Match.class, openedMatch.getCode());
        assertIsClosed(openedMatch);
        assertIsClosed(closedMatch);
        Assertions.assertTrue(closedMatch.getEndDate().getTime() <= now.getTime());
    }

    private void assertIsClosed(Match openedMatch) {
        Match closedMatch = entityManager.find(Match.class, openedMatch.getCode());
        Assertions.assertNotNull(closedMatch);
        Assertions.assertEquals(Status.CLOSE, openedMatch.getStatus());
    }

}

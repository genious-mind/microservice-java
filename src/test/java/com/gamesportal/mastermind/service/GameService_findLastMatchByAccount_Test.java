package com.gamesportal.mastermind.service;

import com.gamesportal.mastermind.entity.Match;
import com.gamesportal.mastermind.enumeration.Status;
import com.gamesportal.mastermind.enumeration.Style;
import com.gamesportal.mastermind.exception.MatchNotFoundException;
import com.gamesportal.mastermind.repository.MatchRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static com.gamesportal.mastermind.MatchTestUtils.*;

@DataJpaTest
public class GameService_findLastMatchByAccount_Test {

    private final TestEntityManager entityManager;
    private final GameService gameService;

    @Autowired
    public GameService_findLastMatchByAccount_Test(TestEntityManager entityManager, MatchRepository matchRepository) {
        this.entityManager = entityManager;
        this.gameService = new GameService(matchRepository);
    }

    @Test
    public void no_match() {
        Assertions.assertThrows(MatchNotFoundException.class, () -> gameService.findLastMatch(ACCOUNT_CODE));
    }

    @Test
    public void close_match() {
        entityManager.persist(createMatch(ACCOUNT_CODE, Status.CLOSE));
        Assertions.assertThrows(MatchNotFoundException.class, () -> gameService.findLastMatch(ACCOUNT_CODE));
    }

    @Test
    public void win_match() {
        entityManager.persist(createMatch(ACCOUNT_CODE, Status.WIN));
        Assertions.assertThrows(MatchNotFoundException.class, () -> gameService.findLastMatch(ACCOUNT_CODE));
    }

    @Test
    public void retrieve_last_match() {
        Match openMatch = createMatch(ACCOUNT_CODE, Style.COLOR, 15, 6);
        entityManager.persist(openMatch);

        Match newMatch = gameService.findLastMatch(ACCOUNT_CODE);

        Assertions.assertEquals(openMatch.getCode(), newMatch.getCode());
        Assertions.assertEquals(openMatch.getStartDate(), newMatch.getStartDate());
        Assertions.assertEquals(openMatch.getEndDate(), newMatch.getEndDate());
        Assertions.assertEquals(openMatch.getStatus(), newMatch.getStatus());
        Assertions.assertEquals(openMatch.getAccountCode(), newMatch.getAccountCode());
        Assertions.assertEquals(openMatch.getStyle(), newMatch.getStyle());
        Assertions.assertEquals(openMatch.getMaxDigit(), newMatch.getMaxDigit());
        Assertions.assertEquals(openMatch.getNumberLength(), newMatch.getNumberLength());
        Assertions.assertEquals(openMatch.getNumberToGuess(), newMatch.getNumberToGuess());
        Assertions.assertEquals(openMatch.getAttempts(), newMatch.getAttempts());
    }

    @Test
    public void retrieve_last_open_match() {
        entityManager.persist(createMatch(ACCOUNT_CODE, Status.CLOSE));
        entityManager.persist(createMatch(ACCOUNT_CODE, Style.COLOR, 15, 6));
        entityManager.persist(createMatch(ACCOUNT_CODE, Status.WIN));

        Match newMatch = gameService.findLastMatch(ACCOUNT_CODE);

        Assertions.assertEquals(Status.OPEN, newMatch.getStatus());
        Assertions.assertEquals(Style.COLOR, newMatch.getStyle());
        Assertions.assertEquals(15, newMatch.getMaxDigit());
        Assertions.assertEquals(6, newMatch.getNumberLength());
    }

    @Test
    public void retrieve_only_my_match() {
        entityManager.persist(createMatch(ACCOUNT_CODE, Status.CLOSE));
        entityManager.persist(createMatch(ANOTHER_ACCOUNT_CODE, Status.OPEN));

        Assertions.assertThrows(MatchNotFoundException.class, () -> gameService.findLastMatch(ACCOUNT_CODE));
    }
}

package com.gamesportal.mastermind.service;

import com.gamesportal.mastermind.entity.Match;
import com.gamesportal.mastermind.enumeration.Status;
import com.gamesportal.mastermind.enumeration.Style;
import com.gamesportal.mastermind.exception.ConfigurationNotValidException;
import com.gamesportal.mastermind.repository.MatchRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.gamesportal.mastermind.MatchTestUtils.ACCOUNT_CODE;
import static com.gamesportal.mastermind.MatchTestUtils.createMatch;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class GameService_createNewMatch_Test {

    private final TestEntityManager entityManager;
    private final GameService gameService;

    @Autowired
    public GameService_createNewMatch_Test(TestEntityManager entityManager, MatchRepository matchRepository) {
        this.entityManager = entityManager;
        this.gameService = new GameService(matchRepository);
    }

    @Test
    public void negative_number() {
        Assertions.assertThrows(ConfigurationNotValidException.class, () -> gameService.createNewMatch(ACCOUNT_CODE, Style.COLOR, -6, 5));
    }

    @Test
    public void invalid_number() {
        Assertions.assertThrows(ConfigurationNotValidException.class, () -> gameService.createNewMatch(ACCOUNT_CODE, Style.COLOR, 7, 9));
    }

    @Test
    public void create_new_match() {
        Match newMatch = gameService.createNewMatch(ACCOUNT_CODE, Style.COLOR, 10, 9);

        Assertions.assertNotNull(newMatch);
        Assertions.assertEquals(Status.OPEN, newMatch.getStatus());
        Assertions.assertEquals(Style.COLOR, newMatch.getStyle());
        Assertions.assertEquals(10, newMatch.getMaxDigit());
        Assertions.assertEquals(9, newMatch.getNumberLength());
    }

    @Test
    public void remove_open_match() {
        Match match = createMatch(ACCOUNT_CODE, Style.NUMBER, 10, 5);
        entityManager.persist(match);

        gameService.createNewMatch(ACCOUNT_CODE, Style.LETTER, 5, 5);

        Match closedMatch = entityManager.find(Match.class, match.getCode());
        Assertions.assertNotNull(closedMatch);
        Assertions.assertEquals(Status.CLOSE, match.getStatus());
    }

}

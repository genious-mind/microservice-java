package com.gamesportal.mastermind.service;

import com.gamesportal.mastermind.entity.Match;
import com.gamesportal.mastermind.enumeration.Status;
import com.gamesportal.mastermind.repository.MatchRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static com.gamesportal.mastermind.MatchTestUtils.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class GameService_closedMatchList_Test {

    private final TestEntityManager entityManager;
    private final GameService gameService;

    @Autowired
    public GameService_closedMatchList_Test(TestEntityManager entityManager, MatchRepository matchRepository) {
        this.entityManager = entityManager;
        this.gameService = new GameService(matchRepository);
    }

    @Test
    public void no_match() {
        List<Match> closedMatchList = gameService.closedMatchList(ACCOUNT_CODE);

        assertThat(closedMatchList, notNullValue());
        assertThat(closedMatchList, hasSize(0));
    }

    @Test
    public void one_open_match() {
        Match match = createMatch(ACCOUNT_CODE, Status.OPEN);
        entityManager.persist(match);

        List<Match> closedMatchList = gameService.closedMatchList(ACCOUNT_CODE);

        assertThat(closedMatchList, notNullValue());
        assertThat(closedMatchList, hasSize(0));
    }

    @Test
    public void one_close_match() {
        Match match = createMatch(ACCOUNT_CODE, Status.CLOSE);
        entityManager.persist(match);

        List<Match> closedMatchList = gameService.closedMatchList(ACCOUNT_CODE);

        assertThat(closedMatchList, notNullValue());
        assertThat(closedMatchList, hasSize(1));
        assertThat(closedMatchList, hasItem(match));
    }

    @Test
    public void one_win_match() {
        Match match = createMatch(ACCOUNT_CODE, Status.WIN);
        entityManager.persist(match);

        List<Match> closedMatchList = gameService.closedMatchList(ACCOUNT_CODE);

        assertThat(closedMatchList, notNullValue());
        assertThat(closedMatchList, hasSize(1));
        assertThat(closedMatchList, hasItem(match));
    }

    @Test
    public void two_close_match() {
        Match match1 = createMatch(ACCOUNT_CODE, Status.CLOSE);
        Match match2 = createMatch(ACCOUNT_CODE, Status.CLOSE);
        entityManager.persist(match1);
        entityManager.persist(match2);

        List<Match> closedMatchList = gameService.closedMatchList(ACCOUNT_CODE);

        assertThat(closedMatchList, notNullValue());
        assertThat(closedMatchList, hasSize(2));
        assertThat(closedMatchList, hasItem(match1));
        assertThat(closedMatchList, hasItem(match2));
    }

    @Test
    public void mixed_match() {
        Match closeMatch1 = createMatch(ACCOUNT_CODE, Status.CLOSE);
        Match winMatch1 = createMatch(ACCOUNT_CODE, Status.WIN);
        Match closeMatch2 = createMatch(ACCOUNT_CODE, Status.CLOSE);
        Match openMatch1 = createMatch(ACCOUNT_CODE, Status.OPEN);
        entityManager.persist(closeMatch1);
        entityManager.persist(winMatch1);
        entityManager.persist(closeMatch2);
        entityManager.persist(openMatch1);

        List<Match> closedMatchList = gameService.closedMatchList(ACCOUNT_CODE);

        assertThat(closedMatchList, notNullValue());
        assertThat(closedMatchList, hasSize(3));
        assertThat(closedMatchList, hasItem(closeMatch1));
        assertThat(closedMatchList, hasItem(winMatch1));
        assertThat(closedMatchList, hasItem(closeMatch2));
    }

    @Test
    public void only_my_match() {
        Match closeMatch1 = createMatch(ACCOUNT_CODE, Status.CLOSE);
        Match winMatch1 = createMatch(ANOTHER_ACCOUNT_CODE, Status.WIN);
        Match closeMatch2 = createMatch(ANOTHER_ACCOUNT_CODE, Status.CLOSE);
        Match winMatch2 = createMatch(ACCOUNT_CODE, Status.WIN);
        Match openMatch1 = createMatch(ACCOUNT_CODE, Status.OPEN);
        Match openMatch2 = createMatch(ANOTHER_ACCOUNT_CODE, Status.OPEN);
        entityManager.persist(closeMatch1);
        entityManager.persist(winMatch1);
        entityManager.persist(closeMatch2);
        entityManager.persist(winMatch2);
        entityManager.persist(openMatch1);
        entityManager.persist(openMatch2);

        List<Match> closedMatchList = gameService.closedMatchList(ACCOUNT_CODE);

        assertThat(closedMatchList, notNullValue());
        assertThat(closedMatchList, hasSize(2));
        assertThat(closedMatchList, hasItem(closeMatch1));
        assertThat(closedMatchList, hasItem(winMatch2));
    }

}

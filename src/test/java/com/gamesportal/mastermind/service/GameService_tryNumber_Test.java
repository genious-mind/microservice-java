package com.gamesportal.mastermind.service;

import com.gamesportal.mastermind.entity.Attempt;
import com.gamesportal.mastermind.entity.Match;
import com.gamesportal.mastermind.enumeration.Status;
import com.gamesportal.mastermind.enumeration.Style;
import com.gamesportal.mastermind.exception.AttemptNotValidException;
import com.gamesportal.mastermind.exception.MatchNotFoundException;
import com.gamesportal.mastermind.repository.MatchRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Comparator;
import java.util.Date;
import java.util.UUID;

import static com.gamesportal.mastermind.MatchTestUtils.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class GameService_tryNumber_Test {

    private final TestEntityManager entityManager;
    private final GameService gameService;

    @Autowired
    public GameService_tryNumber_Test(TestEntityManager entityManager, MatchRepository matchRepository) {
        this.entityManager = entityManager;
        this.gameService = new GameService(matchRepository);
    }

    @Test
    public void cannot_add_attempt_to_no_match() {
        Assertions.assertThrows(MatchNotFoundException.class, () -> gameService.tryNumber(ACCOUNT_CODE, UUID.randomUUID(), new int[]{1, 2, 3, 4, 5}));
    }

    @Test
    public void cannot_add_attempt_to_closed_match() {
        Match closedMatch = entityManager.persist(createMatch(ACCOUNT_CODE, Status.CLOSE));
        Assertions.assertThrows(MatchNotFoundException.class, () -> gameService.tryNumber(ACCOUNT_CODE, closedMatch.getCode(), new int[]{1, 2, 3, 4, 5}));
    }

    @Test
    public void empty_attempt() {
        Match openMatch = entityManager.persist(createMatch(ACCOUNT_CODE, Status.OPEN));
        Assertions.assertThrows(AttemptNotValidException.class, () -> gameService.tryNumber(ACCOUNT_CODE, openMatch.getCode(), new int[]{}));
    }

    @Test
    public void invalid_attempt_length() {
        Match openMatch = entityManager.persist(createMatch(ACCOUNT_CODE, Style.NUMBER, 10, 10));
        Assertions.assertThrows(AttemptNotValidException.class, () -> gameService.tryNumber(ACCOUNT_CODE, openMatch.getCode(), new int[]{1, 2, 3, 4}));
    }

    @Test
    public void invalid_attempt_digit() {
        Match openMatch = entityManager.persist(createMatch(ACCOUNT_CODE, Style.NUMBER, 50, 6));
        Assertions.assertThrows(AttemptNotValidException.class, () -> gameService.tryNumber(ACCOUNT_CODE, openMatch.getCode(), new int[]{1, 99, 3, 4, 5, 6}));
    }

    @Test
    public void insert_one_attempt() {
        Match openMatch = entityManager.persist(createMatch(ACCOUNT_CODE, new int[]{1, 6, 7, 5, 8}));

        Match match = gameService.tryNumber(ACCOUNT_CODE, openMatch.getCode(), new int[]{1, 2, 3, 4, 5});

        assertNotNull(match.getAttempts());
        assertEquals(1, match.getAttempts().size());

        Attempt attempt = match.getAttempts().get(0);
        assertNotNull(attempt.getId());
        assertEquals(attempt.getMatch().getCode(), openMatch.getCode());
        assertTrue(openMatch.getStartDate().getTime() <= attempt.getDate().getTime() && attempt.getDate().getTime() <= new Date().getTime());
        assertArrayEquals(new int[]{1, 2, 3, 4, 5}, attempt.getNumber());
        assertEquals(1, attempt.getExactDigits());
        assertEquals(1, attempt.getContainedDigits());
    }

    @Test
    public void insert_two_attempt() {
        Match match = entityManager.persist(createMatch(ACCOUNT_CODE, new int[]{1, 6, 7, 5, 8}));

        gameService.tryNumber(ACCOUNT_CODE, match.getCode(), new int[]{1, 2, 3, 4, 5});
        gameService.tryNumber(ACCOUNT_CODE, match.getCode(), new int[]{6, 7, 8, 9, 0});

        match = entityManager.find(Match.class, match.getCode());

        assertNotNull(match.getAttempts());
        assertEquals(2, match.getAttempts().size());

        match.getAttempts().sort(Comparator.comparing(Attempt::getDate));
        assertArrayEquals(new int[]{1, 2, 3, 4, 5}, match.getAttempts().get(0).getNumber());
        assertArrayEquals(new int[]{6, 7, 8, 9, 0}, match.getAttempts().get(1).getNumber());
    }

    @Test
    public void win_match() {
        Date now = new Date();
        Match match = entityManager.persist(createMatch(ACCOUNT_CODE, new int[]{1, 6, 7, 5, 8}));

        gameService.tryNumber(ACCOUNT_CODE, match.getCode(), new int[]{1, 6, 7, 5, 8});

        match = entityManager.find(Match.class, match.getCode());

        assertEquals(Status.WIN, match.getStatus());
        assertTrue(now.getTime() <= match.getEndDate().getTime() && match.getEndDate().getTime() <= new Date().getTime());
    }

    @Test
    public void only_my_account() {
        Match openMatch = entityManager.persist(createMatch(ANOTHER_ACCOUNT_CODE, Status.OPEN));
        Assertions.assertThrows(MatchNotFoundException.class, () -> gameService.tryNumber(ACCOUNT_CODE, openMatch.getCode(), new int[]{1, 6, 7, 5, 8}));
    }

    @Test
    public void only_my_match() {
        Match match = entityManager.persist(createMatch(ACCOUNT_CODE, new int[]{1, 6, 7, 5, 8}));
        gameService.tryNumber(ACCOUNT_CODE, match.getCode(), new int[]{1, 6, 7, 5, 2});

        match.setStatus(Status.CLOSE);
        entityManager.persist(match);

        entityManager.persist(createMatch(ACCOUNT_CODE, new int[]{1, 6, 7, 5, 8}));

        Assertions.assertThrows(MatchNotFoundException.class, () -> gameService.tryNumber(ACCOUNT_CODE, match.getCode(), new int[]{1, 6, 7, 5, 8}));
    }

}

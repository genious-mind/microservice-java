package com.gamesportal.mastermind.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Mastermind_constructor_Test {

    @Test
    public void maxDigit_negative() {
        Assertions.assertThrows(NumberFormatException.class, () -> new Mastermind(-10, 5));
    }

    @Test
    public void maxDigit_zero() {
        Assertions.assertThrows(NumberFormatException.class, () -> new Mastermind(0, 5));
    }

    @Test
    public void length_negative() {
        Assertions.assertThrows(NumberFormatException.class, () -> new Mastermind(10, -5));
    }

    @Test
    public void length_zero() {
        Assertions.assertThrows(NumberFormatException.class, () -> new Mastermind(10, 0));
    }

    @Test
    public void length_correct() {
        Assertions.assertDoesNotThrow(() -> new Mastermind(10, 5));
    }

    @Test
    public void maxDigit_greater_numberLength() {
        Assertions.assertThrows(NumberFormatException.class, () -> new Mastermind(5, 10));
    }

    @Test
    public void maxDigit_equals_numberLength() {
        Assertions.assertDoesNotThrow(() -> new Mastermind(10, 10));
    }

    @Test
    public void generate_correct() {
        Mastermind mastermind = new Mastermind(10, 10);
        Assertions.assertDoesNotThrow(() -> mastermind.checkNumberIsValid(mastermind.generateNumber()));
    }

    @Test
    public void generate_length_one() {
        Mastermind mastermind = new Mastermind(10, 1);
        Assertions.assertEquals(1, mastermind.generateNumber().length);
    }

    @Test
    public void generate_length_ten() {
        Mastermind mastermind = new Mastermind(10, 10);
        Assertions.assertEquals(10, mastermind.generateNumber().length);
    }

    @Test
    public void generate_length_ninety_nine() {
        Mastermind mastermind = new Mastermind(99, 99);
        Assertions.assertEquals(99, mastermind.generateNumber().length);
    }

}
package com.gamesportal.mastermind.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class Mastermind_evaluateAttempt_Test {

    private static Mastermind mastermind;

    @BeforeAll
    public static void init() {
        mastermind = new Mastermind(10, 5);
    }

    @Test
    public void noMatch() {
        int[] numberToGuess = new int[]{1, 2, 3, 4, 5};
        int[] attemptedNumber = new int[]{6, 7, 8, 9, 0};
        Mastermind.AttemptEvaluation attempt = mastermind.evaluateAttempt(numberToGuess, attemptedNumber);
        Assertions.assertEquals(0, attempt.getDigitsPresent());
        Assertions.assertEquals(0, attempt.getDigitsExact());
    }

    @Test
    public void oneExact() {
        int[] numberToGuess = new int[]{1, 2, 3, 4, 5};
        int[] attemptedNumber = new int[]{6, 7, 8, 9, 5};
        Mastermind.AttemptEvaluation attempt = mastermind.evaluateAttempt(numberToGuess, attemptedNumber);
        Assertions.assertEquals(0, attempt.getDigitsPresent());
        Assertions.assertEquals(1, attempt.getDigitsExact());
    }

    @Test
    public void onePresent() {
        int[] numberToGuess = new int[]{1, 2, 3, 4, 5};
        int[] attemptedNumber = new int[]{6, 7, 8, 9, 1};
        Mastermind.AttemptEvaluation attempt = mastermind.evaluateAttempt(numberToGuess, attemptedNumber);
        Assertions.assertEquals(1, attempt.getDigitsPresent());
        Assertions.assertEquals(0, attempt.getDigitsExact());
    }

    @Test
    public void onePresent_oneExact() {
        int[] numberToGuess = new int[]{1, 2, 3, 4, 5};
        int[] attemptedNumber = new int[]{1, 3, 6, 7, 8};
        Mastermind.AttemptEvaluation attempt = mastermind.evaluateAttempt(numberToGuess, attemptedNumber);
        Assertions.assertEquals(1, attempt.getDigitsPresent());
        Assertions.assertEquals(1, attempt.getDigitsExact());
    }

    @Test
    public void allExact() {
        int[] numberToGuess = new int[]{1, 2, 3, 7, 5};
        int[] attemptedNumber = new int[]{1, 2, 3, 7, 5};
        Mastermind.AttemptEvaluation attempt = mastermind.evaluateAttempt(numberToGuess, attemptedNumber);
        Assertions.assertEquals(0, attempt.getDigitsPresent());
        Assertions.assertEquals(5, attempt.getDigitsExact());
    }

    @Test
    public void allPresent() {
        int[] numberToGuess = new int[]{1, 2, 3, 7, 5};
        int[] attemptedNumber = new int[]{5, 1, 7, 3, 2};
        Mastermind.AttemptEvaluation attempt = mastermind.evaluateAttempt(numberToGuess, attemptedNumber);
        Assertions.assertEquals(5, attempt.getDigitsPresent());
        Assertions.assertEquals(0, attempt.getDigitsExact());
    }

    @Test
    public void threePresent_twoExact() {
        int[] numberToGuess = new int[]{1, 2, 3, 7, 5};
        int[] attemptedNumber = new int[]{5, 2, 1, 7, 3};
        Mastermind.AttemptEvaluation attempt = mastermind.evaluateAttempt(numberToGuess, attemptedNumber);
        Assertions.assertEquals(3, attempt.getDigitsPresent());
        Assertions.assertEquals(2, attempt.getDigitsExact());
    }

    @Test
    public void twoPresent_twoExact() {
        int[] numberToGuess = new int[]{1, 2, 3, 7, 5};
        int[] attemptedNumber = new int[]{8, 2, 1, 7, 3};
        Mastermind.AttemptEvaluation attempt = mastermind.evaluateAttempt(numberToGuess, attemptedNumber);
        Assertions.assertEquals(2, attempt.getDigitsPresent());
        Assertions.assertEquals(2, attempt.getDigitsExact());
    }

    @Test
    public void isGameOver() {
        int[] numberToGuess = new int[]{1, 2, 3, 7, 5};
        int[] attemptedNumber = new int[]{1, 2, 3, 7, 5};
        Mastermind.AttemptEvaluation attempt = mastermind.evaluateAttempt(numberToGuess, attemptedNumber);
        Assertions.assertTrue(attempt.isGameOver());
    }

    @Test
    public void not_isGameOver() {
        int[] numberToGuess = new int[]{1, 2, 3, 7, 5};
        int[] attemptedNumber = new int[]{8, 2, 1, 7, 3};
        Mastermind.AttemptEvaluation attempt = mastermind.evaluateAttempt(numberToGuess, attemptedNumber);
        Assertions.assertFalse(attempt.isGameOver());
    }

}

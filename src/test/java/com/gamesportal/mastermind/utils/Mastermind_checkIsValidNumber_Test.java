package com.gamesportal.mastermind.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class Mastermind_checkIsValidNumber_Test {

    private static Mastermind mastermind;

    @BeforeAll
    public static void init() {
        mastermind = new Mastermind(10, 5);
    }

    @Test
    public void number_null() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(null));
    }

    @Test
    public void number_empty() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{}));
    }

    @Test
    public void number_length_less() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{1, 2, 3, 4}));
    }

    @Test
    public void number_length_greater() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{1, 2, 3, 4, 5, 6}));
    }

    @Test
    public void number_length_correct() {
        Assertions.assertDoesNotThrow(() -> mastermind.checkNumberIsValid(new int[]{1, 2, 3, 4, 5}));
    }

    @Test
    public void digits_unique() {
        Assertions.assertDoesNotThrow(() -> mastermind.checkNumberIsValid(new int[]{1, 2, 3, 4, 5}));
    }

    @Test
    public void digits_duplicate() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{0, 1, 1, 2, 3}));
    }

    @Test
    public void digits_duplicate_not_consecutive() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{1, 2, 0, 3, 0}));
    }

    @Test
    public void digits_triplicate() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{0, 1, 0, 0, 2}));
    }

    @Test
    public void digit_value_less() {
        Assertions.assertDoesNotThrow(() -> mastermind.checkNumberIsValid(new int[]{5, 6, 7, 8, 9}));
    }

    @Test
    public void digit_value_equals() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{6, 7, 8, 9, 10}));
    }

    @Test
    public void digit_value_great() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{7, 8, 9, 10, 11}));
    }

    @Test
    public void digit_value_great_all() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{21, 33, 98, 15, 11}));
    }

    @Test
    public void digit_value_great_first_and_last() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{12, 8, 9, 10, 11}));
    }

    @Test
    public void digit_value_negative() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{12, 8, -9, 10, 11}));
    }

    @Test
    public void digit_value_negative_all() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{-12, -8, -9, -10, -11}));
    }

    @Test
    public void digit_value_negative_first_last() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{-1, 8, 9, 10, -2}));
    }

    @Test
    public void digit_value_zero() {
        Assertions.assertThrows(NumberFormatException.class, () -> mastermind.checkNumberIsValid(new int[]{1, 8, 0, 10, 2}));
    }

}

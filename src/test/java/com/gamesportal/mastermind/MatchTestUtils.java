package com.gamesportal.mastermind;

import com.gamesportal.mastermind.entity.Match;
import com.gamesportal.mastermind.enumeration.Status;
import com.gamesportal.mastermind.enumeration.Style;
import com.gamesportal.mastermind.utils.Mastermind;

import java.util.Date;
import java.util.UUID;

public class MatchTestUtils {

    public static final UUID ACCOUNT_CODE = UUID.fromString("09eb4637-d98b-4761-8794-7e819a833063");
    public static final UUID ANOTHER_ACCOUNT_CODE = UUID.fromString("c89572da-c07f-4b01-9ddf-b23296fed100");

    public static Match createMatch(UUID accountCode, Status status) {
        Date endDate = Status.OPEN == status ? null : new Date();
        return createMatch(accountCode, status, endDate, Style.NUMBER, 10, 5);
    }

    public static Match createMatch(UUID accountCode, Style style, int maxDigit, int numberLength) {
        return createMatch(accountCode, Status.OPEN, null, style, maxDigit, numberLength);
    }

    public static Match createMatch(UUID accountCode, int[] number) {
        return createMatch(accountCode, number, Status.OPEN, null, Style.NUMBER, 10, 5);
    }

    private static Match createMatch(UUID accountCode, Status status, Date endDate, Style style, int maxDigit, int numberLength) {
        Mastermind mastermind = new Mastermind(maxDigit, numberLength);
        return createMatch(accountCode, mastermind.generateNumber(), status, endDate, style, maxDigit, numberLength);
    }

    private static Match createMatch(UUID accountCode, int[] number, Status status, Date endDate, Style style, int maxDigit, int numberLength) {
        Match match = new Match();
        match.setCode(UUID.randomUUID());
        match.setStartDate(new Date());
        match.setStatus(status);
        match.setEndDate(endDate);
        match.setAccountCode(accountCode);
        match.setNumberToGuess(number);
        match.setStyle(style);
        match.setMaxDigit(maxDigit);
        match.setNumberLength(numberLength);
        return match;
    }

}

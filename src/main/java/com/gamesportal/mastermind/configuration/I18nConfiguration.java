package com.gamesportal.mastermind.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Locale;

@Configuration
public class I18nConfiguration extends AcceptHeaderLocaleResolver implements WebMvcConfigurer {

    private static final Locale DEFAULT_LOCALE = Locale.ENGLISH;

    public I18nConfiguration() {
        Locale.setDefault(DEFAULT_LOCALE);
        setDefaultLocale(DEFAULT_LOCALE);
        setSupportedLocales(Collections.singletonList(DEFAULT_LOCALE));
    }

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String headerLanguage = request.getHeader(HttpHeaders.ACCEPT_LANGUAGE);

        if (null == headerLanguage || headerLanguage.trim().isEmpty()) {
            return Locale.getDefault();
        }

        return Locale.lookup(Locale.LanguageRange.parse(headerLanguage), getSupportedLocales());
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("i18n/messages");
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setUseCodeAsDefaultMessage(true);
        return messageSource;
    }

}

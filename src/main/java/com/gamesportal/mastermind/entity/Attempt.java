package com.gamesportal.mastermind.entity;

import com.gamesportal.mastermind.utils.IntegerArrayToJsonConverter;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity(name = "ATTEMPT")
public class Attempt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "MATCH_CODE", foreignKey = @ForeignKey(name = "FK_ATTEMPT_GAME_MATCH"))
    private Match match;

    @Column(name = "EXACT_DIGITS", nullable = false, updatable = false)
    private int exactDigits;

    @Column(name = "CONTAINED_DIGITS", nullable = false, updatable = false)
    private int containedDigits;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE", nullable = false, updatable = false)
    private Date date;

    @Lob
    @Convert(converter = IntegerArrayToJsonConverter.class)
    @Column(name = "NUMBER", nullable = false)
    private int[] number;

}

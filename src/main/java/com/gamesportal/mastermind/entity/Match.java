package com.gamesportal.mastermind.entity;

import com.gamesportal.mastermind.enumeration.Status;
import com.gamesportal.mastermind.enumeration.Style;
import com.gamesportal.mastermind.utils.IntegerArrayToJsonConverter;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@Entity(name = "GAME_MATCH")
public class Match {

    @Id
    @Type(type = "org.hibernate.type.UUIDCharType")
    @Column(name = "CODE")
    private UUID code;

    @Column(name = "START_DATE", nullable = false)
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = false, length = 10)
    private Status status;

    @Column(name = "ACCOUNT_CODE", nullable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID accountCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "STYLE", nullable = false, length = 10)
    private Style style;

    @Column(name = "MAX_DIGIT", nullable = false)
    private int maxDigit;

    @Column(name = "NUMBER_LENGTH", nullable = false)
    private int numberLength;

    @OneToMany(mappedBy = "match", cascade = CascadeType.ALL)
    private List<Attempt> attempts;

    @Lob
    @Convert(converter = IntegerArrayToJsonConverter.class)
    @Column(name = "NUMBER_TO_GUESS", nullable = false)
    private int[] numberToGuess;

}

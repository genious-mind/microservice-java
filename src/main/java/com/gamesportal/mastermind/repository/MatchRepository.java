package com.gamesportal.mastermind.repository;

import com.gamesportal.mastermind.entity.Match;
import com.gamesportal.mastermind.enumeration.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MatchRepository extends JpaRepository<Match, UUID> {

    Optional<Match> findByAccountCodeAndStatus(UUID accountCode, Status status);

    Optional<Match> findByAccountCodeAndCodeAndStatus(UUID accountCode, UUID code, Status status);

    List<Match> findAllByAccountCodeAndStatus(UUID accountCode, Status status);

    List<Match> findAllByAccountCodeAndStatusNotOrderByStartDateDesc(UUID accountCode, Status status);

}

package com.gamesportal.mastermind.enumeration;

public enum Style {

    NUMBER,
    COLOR,
    LETTER;

}

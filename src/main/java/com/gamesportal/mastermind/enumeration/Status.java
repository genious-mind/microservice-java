package com.gamesportal.mastermind.enumeration;

public enum Status {

    OPEN, WIN, CLOSE;

    public boolean isTerminated() {
        return this != OPEN;
    }

}

package com.gamesportal.mastermind.controller;

import com.gamesportal.mastermind.bean.MatchConfigurationRequest;
import com.gamesportal.mastermind.bean.MatchResponse;
import com.gamesportal.mastermind.bean.TryNumberRequest;
import com.gamesportal.mastermind.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/match")
public class GameController {

    private final GameService service;

    @Autowired
    public GameController(GameService service) {
        this.service = service;
    }

    @GetMapping({"/{accountCode}"})
    public MatchResponse findLastMatch(@PathVariable @NotNull UUID accountCode) {
        return new MatchResponse(service.findLastMatch(accountCode));
    }

    @PostMapping({"/{accountCode}"})
    public MatchResponse createNewMatch(@PathVariable @NotNull UUID accountCode, @RequestBody @Valid MatchConfigurationRequest request) {
        return new MatchResponse(service.createNewMatch(accountCode, request.getStyle(), request.getMaxDigit(), request.getNumberLength()));
    }

    @PutMapping({"/{accountCode}/{matchCode}"})
    public MatchResponse tryNumber(@PathVariable @NotNull UUID accountCode, @PathVariable @NotNull UUID matchCode, @RequestBody @Valid TryNumberRequest request) {
        return new MatchResponse(service.tryNumber(accountCode, matchCode, request.getNumber()));
    }

    @DeleteMapping({"/{accountCode}"})
    public void closeAllOpenMatch(@PathVariable @NotNull UUID accountCode) {
        service.closeAllOpenMatch(accountCode);
    }

    @GetMapping({"/{accountCode}/list"})
    public List<MatchResponse> closedMatchList(@PathVariable @NotNull UUID accountCode) {
        return service.closedMatchList(accountCode).stream().map(MatchResponse::new).collect(Collectors.toList());
    }

}

package com.gamesportal.mastermind.service;

import com.gamesportal.mastermind.entity.Attempt;
import com.gamesportal.mastermind.entity.Match;
import com.gamesportal.mastermind.enumeration.Status;
import com.gamesportal.mastermind.enumeration.Style;
import com.gamesportal.mastermind.exception.AttemptNotValidException;
import com.gamesportal.mastermind.exception.ConfigurationNotValidException;
import com.gamesportal.mastermind.exception.MatchNotFoundException;
import com.gamesportal.mastermind.repository.MatchRepository;
import com.gamesportal.mastermind.utils.Mastermind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class GameService {

    private static final Style DEFAULT_STYLE = Style.NUMBER;
    private static final int DEFAULT_MAX_DIGIT = 10;
    private static final int DEFAULT_NUMBER_LENGTH = 5;

    private final MatchRepository matchRepository;

    @Autowired
    public GameService(final MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    private static Match createMatch(UUID accountCode, Style style, int maxDigit, int numberLength) {
        Mastermind mastermind = new Mastermind(maxDigit, numberLength);

        Match match = new Match();
        match.setCode(UUID.randomUUID());
        match.setStartDate(new Date());
        match.setStatus(Status.OPEN);
        match.setAccountCode(accountCode);
        match.setNumberToGuess(mastermind.generateNumber());
        match.setStyle(style);
        match.setMaxDigit(maxDigit);
        match.setNumberLength(numberLength);
        return match;
    }

    private static Attempt createAttempt(Match match, Mastermind.AttemptEvaluation attemptEvaluation, int[] number) {
        Attempt attempt = new Attempt();
        attempt.setMatch(match);
        attempt.setExactDigits(attemptEvaluation.getDigitsExact());
        attempt.setContainedDigits(attemptEvaluation.getDigitsPresent());
        attempt.setDate(new Date());
        attempt.setNumber(number);
        return attempt;
    }

    public Match findLastMatch(UUID accountCode) {
        return matchRepository.findByAccountCodeAndStatus(accountCode, Status.OPEN)
                .orElseThrow(() -> new MatchNotFoundException(accountCode));
    }

    @Transactional
    public Match tryNumber(UUID accountCode, UUID matchCode, int[] attemptNumber) {
        Match match = matchRepository.findByAccountCodeAndCodeAndStatus(accountCode, matchCode, Status.OPEN)
                .orElseThrow(() -> new MatchNotFoundException(accountCode));

        Mastermind mastermind = new Mastermind(match.getMaxDigit(), match.getNumberLength());

        Mastermind.AttemptEvaluation attempt;
        try {
            attempt = mastermind.evaluateAttempt(match.getNumberToGuess(), attemptNumber);
        } catch (NumberFormatException e) {
            throw new AttemptNotValidException(attemptNumber, match.getCode());
        }

        List<Attempt> attempts = Optional.ofNullable(match.getAttempts()).orElse(new ArrayList<>());
        attempts.add(createAttempt(match, attempt, attemptNumber));
        match.setAttempts(attempts);

        if (attempt.isGameOver()) {
            match.setStatus(Status.WIN);
            match.setEndDate(new Date());
        }

        return matchRepository.save(match);
    }

    @Transactional
    public void closeAllOpenMatch(UUID accountCode) {
        List<Match> openMatchList = closeAllOpenMatchByAccount(accountCode);

        if (openMatchList.isEmpty()) {
            throw new MatchNotFoundException(accountCode);
        }
    }

    @Transactional
    public Match createNewMatch(UUID accountCode, Style style, int maxDigit, int numberLength) {
        try {
            new Mastermind(maxDigit, numberLength);
        } catch (NumberFormatException e) {
            throw new ConfigurationNotValidException(style, maxDigit, numberLength);
        }

        closeAllOpenMatchByAccount(accountCode);

        return matchRepository.save(createMatch(accountCode, style, maxDigit, numberLength));
    }

    public List<Match> closedMatchList(UUID accountCode) {
        return matchRepository.findAllByAccountCodeAndStatusNotOrderByStartDateDesc(accountCode, Status.OPEN);
    }

    private List<Match> closeAllOpenMatchByAccount(UUID accountCode) {
        List<Match> openMatchList = matchRepository.findAllByAccountCodeAndStatus(accountCode, Status.OPEN);

        openMatchList.forEach(match -> {
            match.setStatus(Status.CLOSE);
            match.setEndDate(new Date());
        });

        return matchRepository.saveAll(openMatchList);
    }

}

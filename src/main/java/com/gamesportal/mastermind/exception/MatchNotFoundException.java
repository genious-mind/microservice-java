package com.gamesportal.mastermind.exception;

import org.springframework.http.HttpStatus;

import java.util.UUID;

public class MatchNotFoundException extends GenericException {

    private static final String MESSAGE_KEY = "match.by.account.not.found.exception";

    public MatchNotFoundException(UUID accountCode) {
        super(
                String.format("There are no open match for user '%s'", accountCode),
                HttpStatus.NOT_FOUND,
                MESSAGE_KEY,
                accountCode
        );
    }

}

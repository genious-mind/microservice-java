package com.gamesportal.mastermind.exception;

import com.gamesportal.mastermind.enumeration.Style;
import org.springframework.http.HttpStatus;

public class ConfigurationNotValidException extends GenericException {

    private static final String MESSAGE_KEY = "configuration.not.valid.exception";

    public ConfigurationNotValidException(Style style, int maxDigit, int numberLength) {
        super(
                String.format("The configuration [%s, %d, %d] is not valid!", style, maxDigit, numberLength),
                HttpStatus.BAD_REQUEST,
                MESSAGE_KEY,
                style, maxDigit, numberLength
        );
    }

}

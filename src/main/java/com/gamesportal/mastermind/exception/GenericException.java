package com.gamesportal.mastermind.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public abstract class GenericException extends RuntimeException {

    private final HttpStatus httpStatus;
    private final String messageKey;
    private final Object[] values;

    public GenericException(String errorDescription, HttpStatus httpStatus, String messageKey, Object... values) {
        super(errorDescription);
        this.httpStatus = httpStatus;
        this.messageKey = messageKey;
        this.values = values;
    }

}

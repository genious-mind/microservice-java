package com.gamesportal.mastermind.exception;

import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.UUID;

public class AttemptNotValidException extends GenericException {

    private static final String MESSAGE_KEY = "attempt.not.valid.exception";

    public AttemptNotValidException(int[] attempt, UUID matchCode) {
        super(
                String.format("The attempt %s for match '%s' is not valid!", Arrays.toString(attempt), matchCode),
                HttpStatus.INTERNAL_SERVER_ERROR,
                MESSAGE_KEY,
                Arrays.toString(attempt), matchCode
        );
    }

}

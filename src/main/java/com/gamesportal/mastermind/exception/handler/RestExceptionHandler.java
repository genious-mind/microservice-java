package com.gamesportal.mastermind.exception.handler;

import com.gamesportal.mastermind.exception.GenericException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @Autowired
    public RestExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(value = GenericException.class)
    protected ResponseEntity<Object> handleGenericException(GenericException e, WebRequest request) {
        log.error("", e);

        String message = messageSource.getMessage(e.getMessageKey(), e.getValues(), LocaleContextHolder.getLocale());
        return handleExceptionInternal(e, message, new HttpHeaders(), e.getHttpStatus(), request);
    }

    @ExceptionHandler(value = Exception.class)
    protected ResponseEntity<Object> handleAllException(Exception e, WebRequest request) {
        return handleGenericException(new GenericException(
                "An unhandled exception as occurred!",
                HttpStatus.INTERNAL_SERVER_ERROR,
                "not.handled.exception",
                e.getMessage()) {
        }, request);
    }

}

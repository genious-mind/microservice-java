package com.gamesportal.mastermind.bean;

import com.gamesportal.mastermind.entity.Attempt;
import com.gamesportal.mastermind.entity.Match;
import com.gamesportal.mastermind.enumeration.Status;
import com.gamesportal.mastermind.enumeration.Style;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

@Getter
public class MatchResponse {

    private final UUID code;

    private final Date startDate;

    private final Date endDate;

    private final Status status;

    private final Style style;

    private final int maxDigit;

    private final int numberLength;

    private final UUID accountCode;

    private final List<AttemptResponse> attempts;

    private final int[] number;

    public MatchResponse(Match match) {
        this.code = match.getCode();
        this.startDate = match.getStartDate();
        this.endDate = match.getEndDate();
        this.status = match.getStatus();
        this.style = match.getStyle();
        this.maxDigit = match.getMaxDigit();
        this.numberLength = match.getNumberLength();
        this.number = match.getStatus().isTerminated() ? match.getNumberToGuess() : null;
        this.accountCode = match.getAccountCode();
        this.attempts = Optional.ofNullable(match.getAttempts()).orElse(Collections.emptyList())
                .stream().map(AttemptResponse::new).collect(Collectors.toList());
    }

    @Getter
    private static class AttemptResponse {

        private final int[] number;

        private final int exactDigits;

        private final int containedDigits;

        private final Date date;

        private AttemptResponse(Attempt attempt) {
            number = attempt.getNumber();
            exactDigits = attempt.getExactDigits();
            containedDigits = attempt.getContainedDigits();
            date = attempt.getDate();
        }

    }

}

package com.gamesportal.mastermind.bean;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;

@Getter
public class TryNumberRequest {

    @NotEmpty
    private final int[] number;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public TryNumberRequest(@NotEmpty int[] number) {
        this.number = number;
    }

}

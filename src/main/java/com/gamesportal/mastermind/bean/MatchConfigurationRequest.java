package com.gamesportal.mastermind.bean;

import com.gamesportal.mastermind.enumeration.Style;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MatchConfigurationRequest {

    @NotNull
    private Style style;

    @NotNull
    private Integer maxDigit;

    @NotNull
    private Integer numberLength;

}

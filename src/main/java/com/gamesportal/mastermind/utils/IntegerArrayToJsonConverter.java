package com.gamesportal.mastermind.utils;


import com.fasterxml.jackson.core.type.TypeReference;

public class IntegerArrayToJsonConverter extends JsonConverter<int[]> {

    public IntegerArrayToJsonConverter() {
        super(new TypeReference<int[]>() {
        });
    }

}
package com.gamesportal.mastermind.utils;

import lombok.Getter;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Mastermind {

    private static final int MIN_DIGIT = 0;

    private final int maxDigit;
    private final int numberLength;

    public Mastermind(int maxDigit, int numberLength) {
        checkConfigurationIsValid(maxDigit, numberLength);

        this.maxDigit = maxDigit;
        this.numberLength = numberLength;
    }

    public static void checkConfigurationIsValid(int maxDigits, int numberLength) {
        if (numberLength < 1 || maxDigits < numberLength) {
            throw new NumberFormatException();
        }
    }

    private static boolean areDigitsUniques(int[] number) {
        return getUniqueDigits(number).length == number.length;
    }

    public void checkNumberIsValid(int[] number) {
        if (null == number || number.length != numberLength || !areDigitsUniques(number) || !areAllDigitBetweenMinAndMaxValue(number)) {
            throw new NumberFormatException();
        }
    }

    private static int getRandomDigit(int maxDigit) {
        return ThreadLocalRandom.current().nextInt(0, maxDigit);
    }

    public int[] generateNumber() {
        List<Integer> availableNumber = IntStream.range(MIN_DIGIT, maxDigit).boxed().collect(Collectors.toList());
        int[] number = new int[numberLength];

        for (int i = 0; i < numberLength; i++) {
            int index = getRandomDigit(availableNumber.size());
            number[i] = availableNumber.remove(index);
        }

        return number;
    }

    private static Integer[] getUniqueDigits(int[] number) {
        return IntStream.of(number).boxed().distinct().toArray(Integer[]::new);
    }

    private boolean areAllDigitBetweenMinAndMaxValue(int[] number) {
        return IntStream.of(number).allMatch(digit -> MIN_DIGIT <= digit && digit < maxDigit);
    }

    public AttemptEvaluation evaluateAttempt(int[] numberToGuess, int[] attemptedNumber) {
        return new AttemptEvaluation(numberToGuess, attemptedNumber);
    }

    @Getter
    public class AttemptEvaluation {

        private final int digitsExact;
        private final int digitsPresent;

        private AttemptEvaluation(int[] numberToGuess, int[] attemptedNumber) {
            checkNumberIsValid(numberToGuess);
            checkNumberIsValid(attemptedNumber);

            this.digitsExact = exactPosition(numberToGuess, attemptedNumber);
            this.digitsPresent = samePosition(numberToGuess, attemptedNumber) - this.digitsExact;
        }

        private int samePosition(int[] numberToGuess, int[] number) {
            return (int) IntStream.of(number).filter(digit ->
                    IntStream.of(numberToGuess).anyMatch(digitToGuess -> digitToGuess == digit)
            ).count();
        }

        private int exactPosition(int[] numberToGuess, int[] number) {
            int exactPosition = 0;
            for (int i = 0; i < numberLength; i++) {
                if (numberToGuess[i] == number[i]) {
                    exactPosition++;
                }
            }
            return exactPosition;
        }

        public boolean isGameOver() {
            return digitsExact == numberLength;
        }

    }

}
